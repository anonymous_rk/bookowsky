from rest_framework import serializers
from main.models import Order, Book




class BookSerializer(serializers.ModelSerializer):

    class Meta:
        model = Book
        fields = ['title', 'price', 'published_date', 'author']


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = ['book', 'order_date', 'amount']
