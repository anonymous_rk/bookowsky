from django.db import models
from users.models import CustomUser


class Book(models.Model):
    title = models.CharField(max_length=100, blank=True, null=True)
    author = models.CharField(max_length=50)
    price = models.FloatField()
    published_date = models.DateField()

    def __str__(self):
        return self.title


class Order(models.Model):
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    order_date = models.DateTimeField()
    amount = models.IntegerField(default=0)
    client = models.ForeignKey(CustomUser, on_delete=models.CASCADE, default=2)

    def __str__(self):
        return f'{self.amount} - "{self.book}"s'


# class OrderReport(models.Model):
    #   