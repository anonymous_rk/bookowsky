from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from main.views import OrderListAPIView, OrderDetailAPIView, BookFilterAPIView


urlpatterns = [
    path('', OrderListAPIView.as_view()),
    path('<int:pk>/', OrderDetailAPIView.as_view()),
    path('search/', BookFilterAPIView.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)